﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.vb" Inherits="Elena._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


  <section id="hero" class="wow fadeIn">
    <div class="hero-container">
      <h1>Bienvenido</h1>
      <h2>Esta aplicación ASP realiza una prueba de conexion con el servidor SQL</h2>
      <img src="img/hero-img.png" alt="Hero Imgs">

        <br />
        <asp:Button ID="btn_test" runat="server" Text="Iniciar conexion" class="btn-get-started"/>
        <br />
        <h2><asp:Label ID="lbl_status" runat="server" Text="Esperando inicio de conexion"></asp:Label></h2>

    </div>
  </section><!-- #hero -->

</asp:Content>
